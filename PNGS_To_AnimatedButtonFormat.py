import numpy as np


if __name__ = '__main__':
    
    # COMMAND LINE ARGUMENTS: file.py {zip?} {normal} {hot} {pressed}
    #                                        ^ from 0 to (n-1) imgs ^
    # ALL IMAGE FILES SHOULD BE {same_img_name}{img_num}.{format}
    num_imgs = #files in .zip

    # TODO: import img0
    img0 = import_image0
    # TODO: create description row and append img0
    base = np.ndarray(img0.shape[1])
    base[0] = #SET RGB 65, 78, 77
    base[1] = #SET RGB 66, 84, 78
    base[2] = #SET RGB  1,  0, {num_imgs}
    base[3] = #SET RGB  0,  0, {normal}
    base[4] = #SET RGB  0,  0, {hot}
    base[5] = #SET RGB  0,  0, {pressed}

    # TODO: import the rest of the imgs and append to above
    # TODO: export
    for n in range(num_imgs):
        #TODO: append each img to base
    